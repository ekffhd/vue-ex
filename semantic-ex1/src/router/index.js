import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Main from '../components/Main'
import Channel from '../components/Channel/Channel'
import ChannelContents from '../components/Channel/ChannelContents'
import MyChannel from '../components/Channel/MyChannel'
import Post from '../components/Post'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/channel',
      component: Channel,
      children: [
        {
          path: '/',
          component: ChannelContents,
        }
      ]
    },
    {
      path: '/my_channel',
      component: MyChannel,
    },
    {
      path: '/post',
      component: Post
    }
  ]
})
