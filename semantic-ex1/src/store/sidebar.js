

const state={

}

const getters = {
  sidebarOpen: state => state.sidebarOpen,
  sidebarComponent: state => state.sidebarComponent,
}

const actions = {


}

const mutations = {
  
  SET_SIDEBAR_COMPONENT (state, component) {
    state.sidebarComponent = component
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
