import Vuex from 'vuex'
import sidebar from './sidebar'
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    sidebarOpen: false,
  },

  mutations: {
    TOGGLE_SIDEBAR (state) {
      state.sidebarOpen = !state.sidebarOpen
    },
    SIDEBAR_STATE(state) {
      return state.sidebarOpen
    },
  },

  getters: {

  },
  actions: {
    toggleSidebar (commit) {
      commit(TOGGLE_SIDEBAR)
    }
  }
})

export default{
  store,
}
